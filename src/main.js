// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VueBus from 'vue-bus';
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css';
import App from './App'

import router from './router'
import '@/styles/animation.css'
import '@/styles/main.css'
import '@/styles/element-ui/theme/index.css';

import '@/common/font/font.css'


Vue.config.productionTip = false
Vue.use(VueBus);
Vue.use(ElementUI);


/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
